import bootstrapBundleMin from 'bootstrap/dist/js/bootstrap.bundle.min';
import { useEffect, useState } from 'react';
import InputText from '../components/input-text';
import '../css/post.css';
import { getPostList } from '../services/post';
import { addNewUser, getUserList } from '../services/user';

const Post = () => {

  const [userList, setUserList] = useState([]);
  const [postList, setPostList] = useState([]);
  const [createUser, setCreateUser] = useState({ title: "mr", firstName: "", lastName: "", email: "", picture: "" });
  const { title, firstName, lastName, email, picture } = createUser;

  const getPost = () => {
    getPostList().then(res => {
      setPostList(res)
    });
  }

  const getUser = () => {
    getUserList().then(res => {
      setUserList(res)
    });
  }

  const onCreatePost = () => {
    const body = {
      firstName,
      lastName,
      email,
      title
    }
    addNewUser(body).then(res => {
      showToastSuccess()
    });
  }

  const showToastSuccess = () => {
    var toastHtml = document.getElementById('toastSuccess');

    var toastElement = new bootstrapBundleMin.Toast(toastHtml)
    toastElement.show();
  };

  const showToastFailed = () => {
    var toastHtml = document.getElementById('toastFailed');

    var toastElement = new bootstrapBundleMin.Toast(toastHtml)
    toastElement.show();
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setCreateUser((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  useEffect(() => {
    const fetchData = () => {
      getPost()
      getUser()
    }
    fetchData();
  }, []);

  return (
    <div>
      <div aria-live="polite" aria-atomic="true" className="position-relative" data-bs-autohide="true">
        <div className="toast-container position-absolute top-0 end-0 p-3">

          <div className="toast" id="toastSuccess" role="alert" aria-live="assertive" aria-atomic="true">
            <div className="toast-header d-flex justify-content-between">
              <small className="text-muted">Success</small>
              <button type="button" className="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
            </div>
            <div className="toast-body">

            </div>
          </div>

          <div className="toast" id="toastFailed" role="alert" aria-live="assertive" aria-atomic="true">
            <div className="toast-header d-flex justify-content-between">
              <small className="text-muted">Failed</small>
              <button type="button" className="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
            </div>
            <div className="toast-body">

            </div>
          </div>
        </div>
      </div>

      <div className='user-container text-center mt-5'>
        <div className='mb-3'>
          <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#createPostModal">
            Create Post
          </button>
        </div>
        <div className='d-flex justify-content-center'>
          <table>
            <thead>
              <tr>
                <th>Text</th>
                <th>Tags</th>
                <th>Image</th>
                <th>User</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {
                postList?.data?.data.map((data, index) => {
                  return (
                    <tr key={index}>
                      <td style={{ width: "200px" }}>{data.text} {data.lastName}</td>
                      <td style={{ width: "200px" }}>
                        {
                          data.tags.map((tag, i) => {
                            return (
                              <span key={i}>{tag} </span>
                            )
                          })
                        }
                      </td>
                      <td style={{ width: "200px" }}><img src={data.image} alt={data.picture} className="table-img" /></td>
                      <td style={{ width: "200px" }}>{data.owner.firstName} {data.owner.lastName}</td>
                      <td style={{ width: "200px" }}><button className='btn-edit'>Edit</button> | <button className='btn-delete'>Delete</button></td>
                    </tr>
                  )
                })
              }
            </tbody>
          </table>
        </div>

        <div className="modal fade" id="createPostModal" tabIndex="-1" aria-labelledby="createPostModalLabel" aria-hidden="true">
          <div className="modal-dialog modal-dialog-centered">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="createPostModalLabel">Create Post</h5>
                <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div className="modal-body">
                <div>
                  <select className='w-50' value={title} onChange={handleChange} name='title'>
                    {
                      userList?.data?.data.map((data, index) => {
                        return (
                          <option value={data.id} key={index}>{data.firstName} {data.lastName}</option>
                        )
                      })
                    }
                  </select>
                </div>
                <div>
                  <InputText
                    type='text'
                    value={firstName}
                    name='firstName'
                    className="custom-input-text"
                    placeholder='First Name'
                    onChange={handleChange}
                  />
                </div>
                <div>
                  <InputText
                    type='text'
                    value={lastName}
                    name='lastName'
                    className="custom-input-text"
                    placeholder='Last Name'
                    onChange={handleChange}
                  />
                </div>
                <div>
                  <InputText
                    type='email'
                    value={email}
                    name='email'
                    className="custom-input-text"
                    placeholder='Email'
                    onChange={handleChange}
                  />
                </div>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" className="btn btn-primary" onClick={onCreatePost}>Submit</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Post;
