import bootstrapBundleMin from 'bootstrap/dist/js/bootstrap.bundle.min';
import { useEffect, useState } from 'react';
import InputText from '../components/input-text';
import '../css/user.css';
import { addNewUser, deleteUser, getUserList, updateUser } from '../services/user';

const User = () => {

  const [userList, setUserList] = useState([]);
  const [selectedId, setSelectedId] = useState('');
  const [createUser, setCreateUser] = useState({ title: "mr", firstName: "", lastName: "", email: "", picture: "" });
  const { title, firstName, lastName, email, picture } = createUser;

  const fetchData = () => {
    getUserList().then(res => {
      setUserList(res)
    });
  }

  const onCreateUser = () => {
    const body = {
      firstName,
      lastName,
      email,
      title
    }
    addNewUser(body).then(res => {
      fetchData()
      showToastSuccess();
    });
  }

  const onDeleteUser = (id) => {
    deleteUser(id).then(() => {
      fetchData()
      showToastSuccess()
    });
  }

  const onUpdateUser = () => {
    const body = {
      firstName,
      lastName,
      email,
      title
    }
    updateUser(selectedId, body).then(() => {
      fetchData()
      showToastSuccess()
    });
  }

  const handleChange = (e) => {
    const { name, value } = e.target;
    setCreateUser((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const showToastSuccess = () => {
    var toastHtml = document.getElementById('toastSuccess');

    var toastElement = new bootstrapBundleMin.Toast(toastHtml)
    toastElement.show();
  };

  const showToastFailed = () => {
    var toastHtml = document.getElementById('toastFailed');

    var toastElement = new bootstrapBundleMin.Toast(toastHtml)
    toastElement.show();
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div>
      <div aria-live="polite" aria-atomic="true" className="position-relative" data-bs-autohide="true">
        <div className="toast-container position-absolute top-0 end-0 p-3">

          <div className="toast" id="toastSuccess" role="alert" aria-live="assertive" aria-atomic="true">
            <div className="toast-header d-flex justify-content-between">
              <small className="text-muted">Success</small>
              <button type="button" className="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
            </div>
            <div className="toast-body">

            </div>
          </div>

          <div className="toast" id="toastFailed" role="alert" aria-live="assertive" aria-atomic="true">
            <div className="toast-header d-flex justify-content-between">
              <small className="text-muted">Failed</small>
              <button type="button" className="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
            </div>
            <div className="toast-body">

            </div>
          </div>
        </div>
      </div>

      <div className='user-container text-center mt-5'>
        <div className='mb-3'>
          <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#createUserModal">
            Create User
          </button>
        </div>
        <div className='d-flex justify-content-center'>
          <table>
            <thead>
              <tr>
                <th>Name</th>
                <th>Picture</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {
                userList?.data?.data.map((data, index) => {
                  return (
                    <tr key={index}>
                      <td style={{ width: "200px" }}>{data.firstName} {data.lastName}</td>
                      <td style={{ width: "200px" }}><img src={data.picture} alt={data.picture} className="table-img" /></td>
                      <td style={{ width: "200px" }}><button className='btn-edit' data-bs-toggle="modal" data-bs-target="#updateUserModal" onClick={() => setSelectedId(data.id)}>Edit</button> | <button className='btn-delete' onClick={() => onDeleteUser(data.id)}>Delete</button></td>
                    </tr>
                  )
                })
              }
            </tbody>
          </table>
        </div>

        {/* START OF MODAL CREATE USER */}
        <div className="modal fade" id="createUserModal" tabIndex="-1" aria-labelledby="createUserModalLabel" aria-hidden="true">
          <div className="modal-dialog modal-dialog-centered">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="createUserModalLabel">Create User</h5>
                <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div className="modal-body">
                <div>
                  <select className='w-50' value={title} onChange={handleChange} name='title'>
                    <option value='mr'>mr</option>
                    <option value='mrs'>mrs</option>
                    <option value='miss'>miss</option>
                  </select>
                </div>
                <div>
                  <InputText
                    type='text'
                    value={firstName}
                    name='firstName'
                    className="custom-input-text"
                    placeholder='First Name'
                    onChange={handleChange}
                  />
                </div>
                <div>
                  <InputText
                    type='text'
                    value={lastName}
                    name='lastName'
                    className="custom-input-text"
                    placeholder='Last Name'
                    onChange={handleChange}
                  />
                </div>
                <div>
                  <InputText
                    type='email'
                    value={email}
                    name='email'
                    className="custom-input-text"
                    placeholder='Email'
                    onChange={handleChange}
                  />
                </div>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" className="btn btn-primary" data-bs-dismiss="modal" onClick={onCreateUser}>Submit</button>
              </div>
            </div>
          </div>
        </div>
        {/* END OF MODAL CREATE USER */}

        {/* START OF MODAL UPDATE USER */}
        <div className="modal fade" id="updateUserModal" tabIndex="-1" aria-labelledby="updateUserModalLabel" aria-hidden="true">
          <div className="modal-dialog modal-dialog-centered">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="updateUserModalLabel">Update User</h5>
                <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div className="modal-body">
                <div>
                  <select className='w-50' value={title} onChange={handleChange} name='title'>
                    <option value='mr'>mr</option>
                    <option value='mrs'>mrs</option>
                    <option value='miss'>miss</option>
                  </select>
                </div>
                <div>
                  <InputText
                    type='text'
                    value={firstName}
                    name='firstName'
                    className="custom-input-text"
                    placeholder='First Name'
                    onChange={handleChange}
                  />
                </div>
                <div>
                  <InputText
                    type='text'
                    value={lastName}
                    name='lastName'
                    className="custom-input-text"
                    placeholder='Last Name'
                    onChange={handleChange}
                  />
                </div>
                <div>
                  <InputText
                    type='email'
                    value={email}
                    name='email'
                    className="custom-input-text"
                    placeholder='Email'
                    onChange={handleChange}
                  />
                </div>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" className="btn btn-primary" data-bs-dismiss="modal" onClick={onUpdateUser}>Submit</button>
              </div>
            </div>
          </div>
        </div>
        {/* END OF MODAL UPDATE USER */}
        
      </div>
    </div>
  );
}

export default User;
