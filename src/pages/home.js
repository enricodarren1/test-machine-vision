import { useCallback, useEffect, useState } from 'react';
import InputText from '../components/input-text';
import '../css/home.css';
import { getPostList } from '../services/post';

const Home = () => {

  const [postList, setPostList] = useState([]);
  const [page, setPage] = useState(0);
  const [pageList, setPageList] = useState();
  const [inputValue, setInputValue] = useState({ searchBy: '' });
  const { searchBy } = inputValue;
  let tempSearch = [];

  const fetchData = useCallback((page) => {
    getPostList(page).then(res => {
      setPostList(res.data.data);
      setPageList(Math.round(res.data.total / 20));
    });
  }, [])

  const handleChange = (e) => {
    const { name, value } = e.target;
    setInputValue((prev) => ({
      ...prev,
      [name]: value,
    }));
    
    if (value.length > 1) {
      postList.map(data => {
        return data.tags.filter(tag => {
          if (tag.includes(value)) {
            tempSearch.push(data);
            return setPostList(tempSearch)
          }
        })
      });
    } else {
      getPostList(page).then(res => {
        setPostList(res.data.data);
        setPageList(Math.round(res.data.total / 20));
      });
    }
  };

  const handlePagination = (type) => {
    let currentPage = page;
    if (type === 'next' && currentPage >= 0) {
      setPage(currentPage += 1);
    } else if (type === 'prev' && currentPage > 0) {
      setPage(currentPage -= 1);
    }
    fetchData(currentPage);
  }

  const onSelectPage = (page) => {
    setPage(page);
    fetchData(page);
  }

  useEffect(() => {
    fetchData(page);
  }, [fetchData, page]);

  return (
    <div>
      <div className='mb-3'>
        <InputText
          type='text'
          value={searchBy}
          name='searchBy'
          className="custom-input-text"
          placeholder='Search By Tag'
          onChange={handleChange}
        />
      </div>
      <div className='row'>
        {
          postList.map((data, index) => {
            return (
              <div className='col-3 mb-4' key={index.toString()}>
                <div>
                  <img src={data.owner.picture} alt={data.owner.picture} className='user-img' />
                </div>
                <div className='text-center'>
                  <span className='user-title'>{data.owner.title}. </span><span className='user-name'>{data.owner.firstName} {data.owner.lastName}</span>
                </div>
                <div>
                  <span>{data.text}</span>
                </div>
                <div>
                  {
                    data.tags.map((tag, i) => {
                      return (
                        <span key={i.toString()}>{tag} </span>
                      )
                    })
                  }
                </div>
              </div>
            )
          })
        }
      </div>

      <div>
        <nav aria-label="Page navigation example">
          <ul className="pagination justify-content-center">
            <li className="page-item">
              <a className="page-link" href="#" onClick={() => handlePagination('prev')} tabIndex="-1">Previous</a>
            </li>
            {
              [...Array(pageList)].map((data, i) => {
                return (
                  <li className="page-item" key={i.toString()}><a className="page-link" href="#" onClick={() => onSelectPage(i)}>{i}</a></li>
                )
              })
            }
            <li className="page-item">
              <a className="page-link" onClick={() => handlePagination('next')} href="#">Next</a>
            </li>
          </ul>
        </nav>
      </div>

    </div>
  );
}

export default Home;
