import axios from 'axios';

axios.interceptors.request.use(
  config => {
    config.headers['app-id'] = `62996cb2689bf0731cb00285`;
        return config;
    },
    error => {
        return Promise.reject(error);
    }
);

export const getUserList = () => {
  return axios.get(`https://dummyapi.io/data/v1/user?created=1`);
}

export const addNewUser = (body) => {
  return axios.post(`https://dummyapi.io/data/v1/user/create`, body);
}

export const updateUser = (id, body) => {
  return axios.put(`https://dummyapi.io/data/v1/user/${id}`, body);
}

export const deleteUser = (id) => {
  return axios.delete(`https://dummyapi.io/data/v1/user/${id}`);
}