import axios from 'axios';

axios.interceptors.request.use(
  config => {
    config.headers['app-id'] = `62996cb2689bf0731cb00285`;
        return config;
    },
    error => {
        return Promise.reject(error);
    }
);

export const getPostList = (page) => {
  return axios.get(`https://dummyapi.io/data/v1/post?limit=20&page=${page}&created=1`);
}

export const getPostByUser = (userId) => {
  return axios.get(`https://dummyapi.io/data/v1/user/${userId}/post?limit=20&page=0`);
}