import { BrowserRouter, Link, Route, Switch } from 'react-router-dom';
import '../css/side-bar.css'

const SideBar = (selectedMenu) => {

  const menuList = [
    {
      id: 1,
      title: 'Home',
      path: '/home',
      exact: true
    },
    {
      id: 2,
      title: 'Post',
      path: '/post'
    },
    {
      id: 3,
      title: 'User',
      path: '/user'
    }
  ];

  return (
    <BrowserRouter>
      {
        menuList.map((data, index) => {
          return (
            <div className='menu-item-list' key={index}>
              <Link to={data.path}>
                {data.title}
              </Link>
            </div>
          )
        })
      }
    </BrowserRouter>
  )
};

export default SideBar;