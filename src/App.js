import './App.css';
import { BrowserRouter, Link, Route, Routes } from 'react-router-dom';
import Home from './pages/home';
import User from './pages/user';
import Post from './pages/post';

function App() {
  const menuList = [
    {
      id: 1,
      title: 'Home',
      path: '/home',
      exact: true
    },
    {
      id: 2,
      title: 'User',
      path: '/user'
    },
    {
      id: 3,
      title: 'Post',
      path: '/post'
    }
  ];

  return (
    <BrowserRouter>
      <div>
        <div className='sidebar-container'>
          {
            menuList.map((data, index) => {
              return (
                <div key={index} className="menu-item-list">
                  <Link to={data.path}>{data.title}</Link>
                </div>
              )
            })
          }
        </div>
        <div className='content-container'>
          <Routes>
            <Route exact path="/" element={<Home />} />
            <Route path="/home" element={<Home />} />
            <Route path="/user" element={<User />} />
            <Route path="/post" element={<Post />} />
          </Routes>
        </div>
      </div>
    </BrowserRouter>
  );
}

export default App;
